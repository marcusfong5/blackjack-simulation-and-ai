## BlackJack - Simulation and AI
#### Author: Marcus Fong | Date last edited: 29th Feb 2020
In this project, I was interested in finding out what BlackJack Strategies work
well and how well they would respond to changing conditions.

I first created a simple version of BlackJack where the player has full visibilty
of the dealer's cards. This can be played through the console.

After that, I created the dealer AI to follow normal Casino rules (stand when
below 16 and hit when greater than 16). I also created an AI to play against
the dealer which had two components (Betting AI and Hit/Stand AI).

I implemented some basic strategies for the AI to follow and used matplotlib to 
visualise how well the AI performed. Later on, I would really like to implement
some kind of self-learning into the AI which will watch how I play BlackJack
and try to replicate it. 