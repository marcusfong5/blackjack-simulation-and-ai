import random
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import numpy as np

from termcolor import colored, cprint

# Dictionary

Cards = [('Ace', 'Diamonds'), ('Ace', 'Clubs'), ('Ace', 'Hearts'), ('Ace', 'Spades'),
         ('2', 'Diamonds'), ('2', 'Clubs'), ('2', 'Hearts'), ('2', 'Spades'),
         ('3', 'Diamonds'), ('3', 'Clubs'), ('3', 'Hearts'), ('3', 'Spades'),
         ('4', 'Diamonds'), ('4', 'Clubs'), ('4', 'Hearts'), ('4', 'Spades'),
         ('5', 'Diamonds'), ('5', 'Clubs'), ('5', 'Hearts'), ('5', 'Spades'),
         ('6', 'Diamonds'), ('6', 'Clubs'), ('6', 'Hearts'), ('6', 'Spades'),
         ('7', 'Diamonds'), ('7', 'Clubs'), ('7', 'Hearts'), ('7', 'Spades'),
         ('8', 'Diamonds'), ('8', 'Clubs'), ('8', 'Hearts'), ('8', 'Spades'),
         ('9', 'Diamonds'), ('9', 'Clubs'), ('9', 'Hearts'), ('9', 'Spades'),
         ('10', 'Diamonds'), ('10', 'Clubs'), ('10', 'Hearts'), ('10', 'Spades'),
         ('J', 'Diamonds'), ('J', 'Clubs'), ('J', 'Hearts'), ('J', 'Spades'),
         ('Q', 'Diamonds'), ('Q', 'Clubs'), ('Q', 'Hearts'), ('Q', 'Spades'),
         ('K', 'Diamonds'), ('K', 'Clubs'), ('K', 'Hearts'), ('K', 'Spades'),
         ]
xs = []
for x in range(999):
    xs.append(x)


#########################################################################
# AI and STATS STUFF
def dealerAI(state):
    if valueHand(state, 2) < 16:
        cprint("Dealer Hits!", "yellow")
        newstate = hit(state, 2)
        cprint("Value: " + str(valueHand(state, 2)) + " | Cards: " + str(newstate[2]), "yellow")
        return dealerAI(newstate)
    elif valueHand(state, 2) >= 16 and valueHand(state, 2) <= 21:
        cprint("Dealer Stands", "yellow")
        cprint("Value: " + str(valueHand(state, 2)) + " | Cards: " + str(state[2]), "yellow")
        return state
    elif valueHand(state, 2) > 21:
        cprint("Dealer BUST!", "yellow")


def sample(size):
    if stat[5] < size:
        return "p"
    else:
        return "stat"


def AI(state):
    # If hand is less than dealer hit
    if valueHand(state, 0) < valueHand(state, 2):
        return 'h'
    # If hand is greater than dealer
    elif valueHand(state, 2) <= valueHand(state, 0):
        if valueHand(state, 0) <= 11:
            return 'h'
        else:
            return 's'


def betAI():
    print("winList   " + str(winList))
    print("LoseList  " + str(lostList))
    print("balList   " + str(balList))
    print("betList   " + str(betList))
    print("roundList " + str(roundList))

    if len(lostList) > 1:
        # If there has been a lose
        if lostList[len(lostList) - 1] != lostList[len(lostList) - 2]:
            # If balance is greater than upcoming bet
            if balList[len(balList) - 1] >= 2 * betList[len(betList) - 1]:
                #Then, double previous bet
                return 2 * betList[len(betList) - 1]
            # If balance is less than upcoming bet
            else:
                #Then, bet everything
                return balList[len(balList) - 1]
        # If there has been a win
        else:
            #Then, bet $1
            return '1'
    #If first round
    else:
        #Then, bet $1
        return '1'


    # if valueHand(state, 0) == 20:
    #     return '5'
    # elif valueHand(state, 0) == 21:
    #     return '10'
    # else:
    #     return '1'

#############################################################


# Statistics data
# stat[0]: Wins | stat[1]: Loses | stat[2]: Draws
# stat[4]: Hits | stat[5]: Stands | stat[6]:
# stat = [wins, loses, draws, hits, stands, rounds, bust, money, bet]
stat = [0, 0, 0, 0, 0, 0, 0, 100, 0]

# wins = round, total wins
winList = []
lostList = []
# win-lose ratio
winLose = []
balList = []
betList = []
roundList = []


def isOdd(num):
    if (num % 2) == 0:
        return False
    else:
        return True


def shuffle(deck):
    N = random.randint(0, len(deck) - 1)
    newdeck = []
    for i in range(len(deck) - 1):
        newdeck.append(deck[N])
        deck.remove(deck[N])
        N = random.randint(0, len(deck) - 1)
    return newdeck


def restore(s):
    newsh = [[], [], []]
    for x in range(0, len(s[0])):
        newsh[1].append(s[0][x])
    for y in range(0, len(s[1])):
        newsh[1].append(s[1][y])
    for z in range(0, len(s[2])):
        newsh[1].append(s[2][z])
    return newsh


# playercards = state[0], deck = state[1], dealercards = state[2]
def takeNumber(state, n, player):
    if n > 0:
        # If Player = 0
        if player == 0:
            N = random.randint(0, abs(len(state[1])) - 1)
            state[0].append(state[1][N])
            state[1].remove(state[1][N])
            return takeNumber(state, n - 1, 0)
        # If Dealer = 2
        elif player == 2:
            N = random.randint(0, abs(len(state[1])) - 1)
            state[2].append(state[1][N])
            state[1].remove(state[1][N])
            return takeNumber(state, n - 1, 2)
    else:
        return [state[0], state[1], state[2]]


def hit(state, player):
    return takeNumber(state, 1, player)


def stand(state):
    return state


# State to list of numbers and ACE
# player (0 = player, 1 = deck, 2 = dealer)
def toNumber(state, player):
    total = []
    for i in range(len(state[player])):
        running = state[player][i][0]
        if running == 'K' or running == 'Q' or running == 'J':
            total.append(10)
        elif running == 'Ace':
            total.append(11)
        else:
            total.append(int(running))
    return total


# If Bust
def bust(list):
    if sum(list) > 21:
        return True
    elif sum(list) <= 21:
        return False


# Calculates value of hand
# 'list' denotes toNumber(state) list
def handleAces(list):
    y = []
    sortList = sorted(list, reverse=True)
    for i in range(len(sortList)):
        if bust(sortList):
            if sortList[0] == 11:
                y.append(1)
                sortList.pop(0)
            else:
                y.append(sortList[0])
                sortList.pop(0)
        else:
            y.append(sortList[0])
            sortList.pop(0)
    return y


def valueHand(state, player):
    return sum(handleAces(toNumber(state, player)))


def endround(state, score):
    # player value > dealer value

    if valueHand(state, 0) > valueHand(state, 2) or bust(handleAces(toNumber(state, 2))):
        print("You win!")
        score = [score[0] + 1, score[1]]
        return score
    elif valueHand(state, 2) > valueHand(state, 0) or bust(handleAces(toNumber(state, 0))):
        print("You lose!")
        score = [score[0], score[1] + 1]
        return score
    else:
        return print("Draw!")


# Statespaces:
# 0 : Start Round
# 1 : Dealt cards
# 2 : Bust
# 3 : Stand
# 4 : Lose
# 5 : Win


# start with statespace 0
busted = False
dealerwin = True
statespace = 0
bet = 1
Sn = [[], Cards, []]
while True:
    # Restart round
    while statespace == 0:
        Sn = restore(Sn)
        cprint("-----------------------------------", "cyan")
        cprint("(0) Start Round -- Press (p) to start round", "cyan")
        stat = [stat[0], stat[1], stat[2], stat[3], stat[4], stat[5] + 1, stat[6], stat[7], stat[8]]
        roundList.append(stat[5])
        # resume = input()  # Human Input
        resume = sample(1000)  # AI Input
        if resume == "p":
            Sn = takeNumber(takeNumber(Sn, 2, 2), 2, 0)
            cprint("Dealer's Cards", "yellow", attrs=['bold'])
            cprint("Value: " + str(valueHand(Sn, 2)) + " | Cards: " + str(Sn[2]))
            cprint("My Cards", "green", attrs=['bold'])
            cprint("Value: " + str(valueHand(Sn, 0)) + " | Cards: " + str(Sn[0]), "green")

            statespace = 8
        if resume == "stat" or stat[7] <= 0:
            statespace = 7
            break

    # Deal Cards
    while statespace == 1:
        cprint("(1) Stand/Hit", "cyan")
        choice = AI(Sn)  # AI control
        # choice = input() # Human control
        if choice == 'h':
            Sn = hit(Sn, 0)
            stat = [stat[0], stat[1], stat[2], stat[3] + 1, stat[4], stat[5], stat[6], stat[7], stat[8]]
            if valueHand(Sn, 0) > 21:
                cprint("Value: " + str(valueHand(Sn, 0)) + " | Cards: " + str(Sn[0]), "green")
                statespace = 2
            else:
                cprint("Value: " + str(valueHand(Sn, 0)) + " | Cards: " + str(Sn[0]), "green")
                statespace = 1
        if choice == 's':
            stat = [stat[0], stat[1], stat[2], stat[3], stat[4] + 1, stat[5], stat[6], stat[7], stat[8]]
            statespace = 3
        if choice == "stat":
            statespace = 7
    # Busted
    while statespace == 2:
        cprint("(2) Busted", "cyan")
        stat = [stat[0], stat[1], stat[2], stat[3], stat[4], stat[5], stat[6] + 1, stat[7], stat[8]]
        statespace = 4
    # Stand
    while statespace == 3:
        cprint("(3) Stand", "cyan")
        dealerAI(Sn)
        if valueHand(Sn, 0) < valueHand(Sn, 2) <= 21:
            statespace = 4
        elif valueHand(Sn, 0) > valueHand(Sn, 2) or valueHand(Sn, 2) > 21:
            statespace = 5
        elif valueHand(Sn, 2) == valueHand(Sn, 0):
            statespace = 6
    # Lose
    while statespace == 4:
        cprint("(4) Lost", "cyan")
        stat = [stat[0], stat[1] + 1, stat[2], stat[3], stat[4], stat[5], stat[6], stat[7] - stat[8], stat[8] - stat[8]]
        winList.append(stat[0])
        lostList.append(stat[1])
        balList.append(stat[7])
        statespace = 0
    # Win
    while statespace == 5:
        cprint("(5) Win", "cyan")
        stat = [stat[0] + 1, stat[1], stat[2], stat[3], stat[4], stat[5], stat[6], stat[7] + stat[8], stat[8] - stat[8]]
        winList.append(stat[0])
        lostList.append(stat[1])
        balList.append(stat[7])
        statespace = 0
    # Draw
    while statespace == 6:
        cprint("(6) Draw", "cyan")
        stat = [stat[0], stat[1], stat[2] + 1, stat[3], stat[4], stat[5], stat[6], stat[7], stat[8] - stat[8]]
        winList.append(stat[0])
        lostList.append(stat[1])
        balList.append(stat[7])
        statespace = 0

    # Scoreboard
    while statespace == 7:
        cprint("================================", "cyan")
        cprint("(7) Stats", "cyan")
        cprint("--------------------------------", "cyan")
        cprint("Wins:     " + str(stat[0]) + "  | Hits:    " + str(stat[3]), "cyan")
        cprint("Loses:    " + str(stat[1]) + "  | Stands:  " + str(stat[4]), "cyan")
        cprint("Draws:    " + str(stat[2]) + "  | Rounds:  " + str(stat[5] - 1), "cyan")
        cprint("Busts:    " + str(stat[6]) + "  | Money:   " + str(stat[7]), "cyan")
        cprint("================================", "cyan")
        cprint("Metrics", "cyan")
        cprint("--------------------------------", "cyan")
        cprint("Bust/Lose: " + str(round((stat[6]/stat[1]), 2)*100) + "%", "cyan")
        cprint("Win/Lose: " + str(round((stat[0]/stat[1]), 2)*100) + "%", "cyan")
        for x in winList:
            if winList[x] != 0 and lostList[x] != 0:
                winLose.append(winList[x] / lostList[x])
            else:
                winLose.append(0.0)
        plt.style.use("dark_background")
        for x in range(len(balList) - 1):
            # -- Win/Lose Ratio
            # plt.bar(x, winLose[x], width=0.8, color='red')
            # plt.xlabel("Round Number")
            # plt.ylabel("Win-Lose Ratio")
            # plt.title("BlackJack Win/Lose over time")
            # plt.pause(0.001)
            # plt.ylim(0, 2.0)
            # plt.xlim(max(x - 50, 0), max(x + 5, 50))

            # -- Money
            plt.bar(x, balList[x], width=0.8, color='red')
            plt.xlabel("Round Number")
            plt.ylabel("Balance")
            plt.title("BlackJack Money over time")
            plt.pause(0.001)
            if balList[x] >= 500:
                plt.ylim(500, 1000)
            else:
                plt.ylim(0, 500)
            plt.xlim(max(x - 50, 0), max(x + 5, 50))

        plt.show()

    # Betting
    while statespace == 8:
        cprint("Balance: " + str(stat[7]) + " | Place bet: ", "cyan")
        # bet = input()
        bet = betAI()
        if str.isdigit(str(bet)) and stat[7] >= int(bet) >= 1:
            stat = [stat[0], stat[1], stat[2], stat[3], stat[4], stat[5], stat[6], stat[7], stat[8] + int(bet)]
            betList.append(stat[8])
            cprint("$" + str(bet) + " placed", "green")
            cprint("------------------------------", "cyan")
            statespace = 1
        else:
            cprint("Invalid Input. Try again", "red")

###########################